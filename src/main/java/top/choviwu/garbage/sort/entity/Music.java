package top.choviwu.garbage.sort.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 奕仁
 * @since 2020-01-05
 */
@Data
@EqualsAndHashCode
public class Music implements Serializable {

    private static final long serialVersionUID=1L;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String songId;

    private String title;

    private String author;
    private Integer state;

    private String lrc;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String region;

    private String url;

    private String pic;

    private String link;

    private LocalDateTime addtime;

    private String addip;


    @Override
    public String toString() {
        return "Music{" +
        "id=" + id +
        ", songId=" + songId +
        ", title=" + title +
        ", author=" + author +
        ", lrc=" + lrc +
        ", region=" + region +
        ", url=" + url +
        ", pic=" + pic +
        ", link=" + link +
        ", addtime=" + addtime +
        ", addip=" + addip +
        "}";
    }
}
