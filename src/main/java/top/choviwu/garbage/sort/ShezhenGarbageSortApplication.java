package top.choviwu.garbage.sort;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import top.choviwu.garbage.sort.mapper.ConfigMapper;
import top.choviwu.garbage.sort.mapper.GarbageMapper;
import top.choviwu.garbage.sort.redis.RedisRepository;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.text.DateFormat;


@EnableTransactionManagement
@EnableRedisRepositories
@EnableAspectJAutoProxy(proxyTargetClass = true,exposeProxy = true)
@SpringBootApplication
@EnableAsync(proxyTargetClass = true)
@EnableScheduling
@MapperScan(basePackages = "top.choviwu.garbage.sort.mapper")
public class ShezhenGarbageSortApplication implements ServletContextInitializer {

    @Autowired
    ConfigMapper configMapper;
    @Autowired
    GarbageMapper garbageMapper;
    @Autowired
    RedisRepository cached;
//    @Autowired
//    SpiderQuaz spiderQuaz;

    public static void main(String[] args) throws InterruptedException {

        ApplicationContext context = SpringApplication.run(ShezhenGarbageSortApplication.class, args);


    }


    @Bean
    public HttpMessageConverter message() {
        HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        ObjectMapper ob = new ObjectMapper();
        ob.setDateFormat(DateFormat.getInstance());
        ((MappingJackson2HttpMessageConverter) converter).setObjectMapper(ob);
        return converter;
    }


    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
//        List<Config> list = configMapper.selectList(Wrappers.emptyWrapper());
//        list.stream().filter(c -> c.getParam().contains("baidu"))
//                .forEach(c -> CACHED.put(c.getParam(), c.getResult()));
//        list.forEach(c->CONFIG.put(c.getParam(),c.getResult()));
//        Thread thread = new Thread(spiderQuaz);
//        thread.setDaemon(true);
//        thread.start();
        //
//        garbageMapper.selectList(Wrappers.emptyWrapper())
//                .forEach(c->{
//                    cached.sSet(Cached.GARBAGE_LIST, JsonUtils.toJson(c));
//                    cached.hset(Cached.GARBAGE_HASH,c.getGName(), JsonUtils.toJson(c));
//        });
    }
}
