package top.choviwu.garbage.sort.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;
import top.choviwu.garbage.sort.annotation.GLog;
import top.choviwu.garbage.sort.annotation.Limit;
import top.choviwu.garbage.sort.annotation.LogType;
import top.choviwu.garbage.sort.entity.Music;
import top.choviwu.garbage.sort.ex.CrudException;
import top.choviwu.garbage.sort.service.MusicService;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 奕仁
 * @since 2020-01-05
 */
@Controller
@RequestMapping("/api")
public class MusicController {

    @Autowired
    private MusicService musicService;

    @Limit(limits = 5)
    @GLog(LogType.searchSong)
    @ResponseBody
    @RequestMapping(value = "/getMusic")
    public Object getMusic(@RequestParam(required = false) String name){
        if(StringUtils.isEmpty(name)){
            throw new CrudException("歌名不能为空");
        }

        return   musicService.getMusic(name.trim());

    }

}

