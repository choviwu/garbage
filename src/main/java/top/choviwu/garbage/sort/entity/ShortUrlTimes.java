package top.choviwu.garbage.sort.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 奕仁
 * @since 2019-09-14
 */
public class ShortUrlTimes implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer keyId;

    private Integer openTimes;

    private LocalDateTime addtime;

    private LocalDateTime updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getKeyId() {
        return keyId;
    }

    public void setKeyId(Integer keyId) {
        this.keyId = keyId;
    }

    public Integer getOpenTimes() {
        return openTimes;
    }

    public void setOpenTimes(Integer openTimes) {
        this.openTimes = openTimes;
    }

    public LocalDateTime getAddtime() {
        return addtime;
    }

    public void setAddtime(LocalDateTime addtime) {
        this.addtime = addtime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "ShortUrlTimes{" +
        "id=" + id +
        ", keyId=" + keyId +
        ", openTimes=" + openTimes +
        ", addtime=" + addtime +
        ", updateTime=" + updateTime +
        "}";
    }
}
