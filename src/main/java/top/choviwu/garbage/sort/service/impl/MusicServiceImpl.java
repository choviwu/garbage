package top.choviwu.garbage.sort.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import top.choviwu.garbage.sort.entity.Music;
import top.choviwu.garbage.sort.entity.bean.MusicVo;
import top.choviwu.garbage.sort.ex.CrudException;
import top.choviwu.garbage.sort.mapper.MusicMapper;
import top.choviwu.garbage.sort.service.MusicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.choviwu.garbage.sort.util.IpUtils;
import top.choviwu.garbage.sort.util.MusicUtil;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 奕仁
 * @since 2020-01-05
 */
@Service
public class MusicServiceImpl extends ServiceImpl<MusicMapper, Music> implements MusicService {


    @Autowired
    private MusicMapper musicMapper;

    @Override
    public List<Music> getMusic(String music){
        try {
            List<Music> list = musicMapper.selectList(new QueryWrapper<Music>().eq("state",0).like(true,"title",music));
            if(list!=null &&!list.isEmpty()){
               filter(list);
                return list.stream().limit(10).collect(Collectors.toList());
            }
            List<MusicVo> mapList  = MusicUtil.getMusic(music,0);
            if(mapList!=null){
                list =  mapList.stream().map(this::parseProperties).collect(Collectors.toList());
                saveMusic(list);
                filter(list);
                return list.stream().limit(10).collect(Collectors.toList());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        throw new CrudException("没有找到该歌曲!");
    }
    @Async
    public void saveMusic(Collection<Music> list){
        for (Music music : list) {
            if(musicMapper.selectCount(new QueryWrapper<Music>().eq("url",music.getUrl()))==0){
                this.save(music);
            }
        }

    }


    private Music parseProperties(MusicVo musicVo){
        Music music = new Music();
        music.setAddip(IpUtils.getIpAddr(((ServletRequestAttributes)(RequestContextHolder.currentRequestAttributes())).getRequest()));
        music.setAddtime(LocalDateTime.now());
        music.setAuthor(musicVo.getAuthor());
        music.setLink("http://music.163.com/#/song?id="+musicVo.getId());
        music.setSongId(musicVo.getId());
        music.setTitle(musicVo.getTitle());
        music.setUrl(musicVo.getUrl());
        music.setPic(musicVo.getPic());
        music.setRegion("网易云");
        music.setState(0);
        return music;
    }

    private void filter(List<Music> list){
        list.forEach(c->{
            c.setAddtime(null);
            c.setId(null);
            c.setAddip("");
        });
    }





}
