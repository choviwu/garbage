package top.choviwu.garbage.sort.service;

import top.choviwu.garbage.sort.entity.ShortUrlTimes;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 奕仁
 * @since 2019-09-14
 */
public interface ShortUrlTimesService extends IService<ShortUrlTimes> {

}
