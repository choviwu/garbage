package top.choviwu.garbage.sort.mapper;

import org.apache.ibatis.annotations.Param;
import top.choviwu.garbage.sort.entity.Famous;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 奕仁
 * @since 2020-02-01
 */
public interface FamousMapper extends BaseMapper<Famous> {


    Famous getOneFamous(@Param("amount") Integer amount);
}
