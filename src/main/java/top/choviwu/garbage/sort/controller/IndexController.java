package top.choviwu.garbage.sort.controller;

import cn.hutool.http.HttpUtil;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.choviwu.garbage.sort.test.SpiderQuaz;

import javax.servlet.http.HttpServletRequest;

@Controller
public class IndexController {

    @Autowired
    SpiderQuaz spiderQuaz;

    @ResponseBody
    @HystrixCommand(commandKey = "executeSpider",
            fallbackMethod = "fallBackMethod",
            threadPoolKey = "getIndexByThread",
            commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds" , value = "3000"),//超时时间
            @HystrixProperty(name = "circuitBreaker.enabled" , value = "true"),///设置熔断
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold" , value = "10"),////断路器的最小请求数
            },ignoreExceptions = {ArithmeticException.class})
    @RequestMapping("/executeSpider")
    public String index(HttpServletRequest request){
//        Thread thread = new Thread(spiderQuaz);
//        thread.setDaemon(true);
//        thread.start();
        return request.getRemoteAddr();
    }



    @HystrixCommand(commandKey = "index",
            fallbackMethod = "fallBackMethod",
            threadPoolKey = "getIndexByThread",
            commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds" , value = "3000"),//超时时间
            @HystrixProperty(name = "circuitBreaker.enabled" , value = "true"),///设置熔断
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold" , value = "10"),////断路器的最小请求数
            })
    @RequestMapping("/index")
    public String indexPage(HttpServletRequest request){
       return "index";
    }


    public String fallBackMethod(HttpServletRequest request,Throwable e){
        e.printStackTrace();
        return "error";
    }
}
