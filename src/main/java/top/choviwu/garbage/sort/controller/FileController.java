package top.choviwu.garbage.sort.controller;


import cn.hutool.core.util.RandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import top.choviwu.garbage.sort.service.UserService;
import top.choviwu.garbage.sort.util.CusAccessObjectUtil;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 奕仁
 * @since 2019-07-21
 */
@RestController
@RequestMapping("/api")
public class FileController {



    @Autowired
    private UserService userService;

    @PostMapping("/upload")
    public Object register(MultipartFile multipartFile)throws Exception{
        long l = RandomUtil.getRandom(true).nextLong();

        Path path = Paths.get(System.getProperty("user.dir") + "/" + l + ".jpg");
        Path filePath = Files.createFile(path);
        File file = filePath.toFile();
        multipartFile.transferTo(file);
        return  l + ".jpg";
    }

}

