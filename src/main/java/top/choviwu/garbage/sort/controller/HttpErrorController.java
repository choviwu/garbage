package top.choviwu.garbage.sort.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
//@Controller
public class HttpErrorController implements ErrorController {

    private final static String ERROR_PATH = "/error";


    @RequestMapping(path  = ERROR_PATH )
    public Object error(HttpServletRequest request, HttpServletResponse response){
        log.info("访问/error" + "  错误代码："  + response.getStatus());
        return "/views/index.html";
    }
    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }
}