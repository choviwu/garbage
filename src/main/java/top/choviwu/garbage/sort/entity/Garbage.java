package top.choviwu.garbage.sort.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import top.choviwu.garbage.sort.constant.GsCode;

import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 奕仁
 * @since 2019-07-20
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Garbage implements Serializable {

    private static final long serialVersionUID=1L;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String gName;

    private String gType;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private LocalDateTime gTime;

    public static Garbage defaultObj(String gName){
        return new Garbage(0,gName, GsCode.UNKNOW.getValue(),LocalDateTime.now());
    }
}
