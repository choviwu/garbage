package top.choviwu.garbage.sort.entity;

import lombok.Data;

@Data
public class BaseGarbage {

    private String app_id;
    private long time_stamp;
    private int format;
    private int topk;
    private String  image;
    private String  sign;
}
