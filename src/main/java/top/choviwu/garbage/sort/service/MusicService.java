package top.choviwu.garbage.sort.service;

import top.choviwu.garbage.sort.entity.Music;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 奕仁
 * @since 2020-01-05
 */
public interface MusicService extends IService<Music> {

    List<Music> getMusic(String music);
}
