package top.choviwu.garbage.sort.mapper;

import top.choviwu.garbage.sort.entity.ShortUrlTimes;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 奕仁
 * @since 2019-09-14
 */
public interface ShortUrlTimesMapper extends BaseMapper<ShortUrlTimes> {

}
