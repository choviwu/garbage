package top.choviwu.garbage.sort;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

public class ShezhenGarbageSortApplicationTests {

    @Test
    public void contextLoads() {
         int num []= new int[]{3,3};
         int target = 6;
        System.out.println(twoSum(num,target));

    }

    public int[] twoSum(int[] nums, int target) {
        int temp = 0;
        int result[] = new int[2];
        outer: for(int i = 0;i<nums.length;i++){
            for(int j = nums.length-1;j>0;j--){
                if(i == j){
                    continue ;
                }
                temp = nums[j]+nums[i];
                if(temp==target){
                    result[0] = j;
                    result[1] = i;
                    break outer;
                }
            }
        }
        return result;
    }

}
