package top.choviwu.garbage.sort.service.impl;

import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import top.choviwu.garbage.sort.constant.Constants;
import top.choviwu.garbage.sort.entity.Famous;
import top.choviwu.garbage.sort.mapper.FamousMapper;
import top.choviwu.garbage.sort.service.FamousService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.choviwu.garbage.sort.util.JsonUtils;
import top.choviwu.garbage.sort.util.SpriderUtils;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 奕仁
 * @since 2020-02-01
 */
@Service
public class FamousServiceImpl extends ServiceImpl<FamousMapper, Famous> implements FamousService {

    @Resource
    private FamousMapper famousMapper;

    private static final Executor SINGLE = Executors.newSingleThreadExecutor();
    @Override
    public String getFamous(String ip,final int amount) {

        Famous famous = famousMapper.getOneFamous(amount);
        SINGLE.execute(()->{
            famous.setLastPushTime(LocalDateTime.now());
            famousMapper.updateById(famous);
        });
        return famous.getContent();
    }



}
