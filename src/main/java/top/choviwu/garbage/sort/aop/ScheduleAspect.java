package top.choviwu.garbage.sort.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import top.choviwu.garbage.sort.constant.Constants;
import top.choviwu.garbage.sort.mapper.ConfigMapper;

import java.util.Objects;

//@Aspect
//@Component
public class ScheduleAspect {

    private ConfigMapper configMapper;

    @Pointcut(value = "@annotation(org.springframework.scheduling.annotation.Scheduled)")
    public void pointCut(){}

    @Around(value = "pointCut()&&@annotation(scheduled)")
    public Object around(ProceedingJoinPoint joinPoint, Scheduled scheduled) throws Throwable {
//        if(Boolean.parseBoolean(Constants.CONFIG.get("schedule_flag"))){
//            return joinPoint.proceed();
//        }
        return null;
    }
}
