package top.choviwu.garbage.sort.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 奕仁
 * @since 2020-02-01
 */
@Data
@Accessors(chain = true)
public class Famous implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String title;

    private String content;

    private Integer type;

    private LocalDateTime lastPushTime;

    @Override
    public String toString() {
        return "Famous{" +
        "id=" + id +
        ", title=" + title +
        ", content=" + content +
        ", type=" + type +
        ", lastPushTime=" + lastPushTime +
        "}";
    }
}
