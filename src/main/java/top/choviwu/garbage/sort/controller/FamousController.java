package top.choviwu.garbage.sort.controller;


import cn.hutool.core.map.MapUtil;
import com.google.common.collect.Maps;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;
import top.choviwu.garbage.sort.constant.Constants;
import top.choviwu.garbage.sort.service.FamousService;
import top.choviwu.garbage.sort.util.IpUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 奕仁
 * @since 2020-02-01
 */
@RestController
public class FamousController {


    @Resource
    private FamousService famousService;

    @RequestMapping(value = "/day")
    public Object day(HttpServletRequest request){
        final String ret = famousService.getFamous(IpUtils.getIpAddr(request), Constants.DAY);
        return ret;
    }
    @RequestMapping("/week")
    public Object week(HttpServletRequest request){
        final String ret = famousService.getFamous(IpUtils.getIpAddr(request),Constants.WEEK);
        return ret;
    }
}

