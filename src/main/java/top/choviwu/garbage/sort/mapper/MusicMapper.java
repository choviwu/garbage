package top.choviwu.garbage.sort.mapper;

import top.choviwu.garbage.sort.entity.Music;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 奕仁
 * @since 2020-01-05
 */
public interface MusicMapper extends BaseMapper<Music> {

}
