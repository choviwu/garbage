function generator() {
    var content = $("#content").val();
    $.get('/genShortUrl?url='+content,
        function (ret) {
            console.log(ret)
            if(ret.code==200){
                //在这里面输入任何合法的js语句
                layer.open({
                    type: 1 //Page层类型
                    ,area: ['300px', '150px']
                    ,title: '生成成功'
                    ,shade: 0.6 //遮罩透明度
                    ,maxmin: true //允许全屏最小化
                    ,anim: 1 //0-6的动画形式，-1不开启
                    ,content: '<div id="copy_btn" data-clipboard-action="copy" data-clipboard-target="#resultDv">'+'<span id="resultDv">'+ret.data+'</span>'+'</div>'
                });
                /*$("#qrcode").css("display",'block');
                $(".tiny-url-qrcode").attr('src',ret.data.qrCode)*/
            }
        }
    )
}
function queryMusic() {
    var title = $("input[name=title]").val();
    $.get('/music/?name='+title,
        function (ret) {
            console.log(ret)
            if(ret.code==200){
                //在这里面输入任何合法的js语句
                if(ret.data.length>0){
                    var arr1 = ret.data;
                    var music = "";
                    $.each(arr1, function (i, val) {
                        music += "歌曲："+val.title +
                            " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a target='_blank' href="+val.link+ ">点击跳转至网易云地址</a>" +  '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a target="_blank" href="'+val.url+'">点击下载</a> ' +
                            // '<input id="popBtn'+i+'" type="button" onclick="playmp3(\''+val.url+'\')" value="播放"/> ' +
                            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:blue;">作者：'+val.author+'</span> <br>';
                    });
                    $(".post-title").empty();
                    $(".post-title").append('<input id="btn" type="button" onclick="playmp3('+arr1[0].url+')" value="开始"/>');
                    $(".post-title").append('<audio id="audio1" controls="controls">' +
                        '    <source id="audioSource" src="'+arr1[0].url+'" type="audio/ogg">' +
                        '    ' +arr1[0].title+
                        '</audio>');
                    //在这里面输入任何合法的js语句
                    layer.open({
                        title: '<span style="font-size:13px">查询结果(<span style="color: red;">温馨提示：下载歌曲可以复制到手机浏览器中…</span>)</span>',
                        type: 1 //Page层类型
                        ,area : ['100%', '80%']
                        ,shade: 0.6 //遮罩透明度
                        ,maxmin: true //允许全屏最小化
                        ,anim: 1 //0-6的动画形式，-1不开启
                        ,content: '<div style="padding:50px;">'+music.toString()+'</div>'
                    });
                    // var audio = document.getElementById('audio1');

                    // if (audio !== null) {
                    //     audio.play();//audio.play();// 这个就是播放
                    // }
                }

                /*$("#qrcode").css("display",'block');
                $(".tiny-url-qrcode").attr('src',ret.data.qrCode)*/
            }else{
                layer.open({
                    title: '查询结果',
                    type: 1 //Page层类型
                    ,area : ['100%', '100%']
                    ,shade: 0.6 //遮罩透明度
                    ,maxmin: true //允许全屏最小化
                    ,anim: 1 //0-6的动画形式，-1不开启
                    ,content: '<div style="padding:50px;">'+ret.data+'</div>'
                });
            }
        }
    )
}
function playmp3(url) {
    $("#audioSource").attr('src',url);
    var audio = document.getElementById('audio1');
    audio.play();//audio.play();// 这个就是播放
}

function showCopy(){
    layer.open({
        title: '版权说明'
        ,type: 1 //Page层类型
        ,area: ['600px', '300px'],
        shade: 0.6 //遮罩透明度
        ,maxmin: true //允许全屏最小化
        ,anim: 1 //0-6的动画形式，-1不开启
        ,content: '本站音频文件来自各网站接口，本站不会修改任何音频文件音频版权来自网易云<br/>' +
            '本站只提供数据查询服务，不提供任何音频存储和贩卖服务,如有侵权，请联系站长删除资源<br/>' +
            '联系方式：<a target="_blank" href="tencent://message/?uin=2802851919&amp;Site=&amp;Menu=yes"><i class="fa fa-qq"></i>奕仁</a>'
    });
}
$(document).ready(function(){
    var clipboard = new Clipboard('#copy_btn');
    clipboard.on('success', function(e) {
        layer.msg("复制成功");
        e.clearSelection();
        console.log(e.clearSelection);
    });
});