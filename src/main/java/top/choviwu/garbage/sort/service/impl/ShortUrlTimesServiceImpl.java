package top.choviwu.garbage.sort.service.impl;

import top.choviwu.garbage.sort.entity.ShortUrlTimes;
import top.choviwu.garbage.sort.mapper.ShortUrlTimesMapper;
import top.choviwu.garbage.sort.service.ShortUrlTimesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 奕仁
 * @since 2019-09-14
 */
@Service
public class ShortUrlTimesServiceImpl extends ServiceImpl<ShortUrlTimesMapper, ShortUrlTimes> implements ShortUrlTimesService {

}
