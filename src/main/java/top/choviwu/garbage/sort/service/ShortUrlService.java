package top.choviwu.garbage.sort.service;

import top.choviwu.garbage.sort.entity.ShortUrl;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 奕仁
 * @since 2019-09-14
 */
public interface ShortUrlService extends IService<ShortUrl> {

}
