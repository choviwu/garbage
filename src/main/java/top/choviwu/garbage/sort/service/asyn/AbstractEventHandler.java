package top.choviwu.garbage.sort.service.asyn;


/**
* @Description: {@link EventQueue#EventQueue(AbstractEventHandler, int)}
* @author choviwu choviwu@163.com
* @date 2019/8/10  14:11
*/

public abstract class AbstractEventHandler<E>  implements
		Runnable {

	private EventQueue<E> queue;

	@Override
	public void run() {

		E event;
		synchronized (queue) {
			event = queue.poll();
		}
		if (null != event){
			handle(event);
		}
	}

	public abstract void handle(E event);

	protected void setQueue(EventQueue<E> queue) {

		this.queue = queue;

	}

}
