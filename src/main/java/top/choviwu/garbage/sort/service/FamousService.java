package top.choviwu.garbage.sort.service;

import top.choviwu.garbage.sort.entity.Famous;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 奕仁
 * @since 2020-02-01
 */
public interface FamousService extends IService<Famous> {


    String getFamous(String ip,final int amount);
}
