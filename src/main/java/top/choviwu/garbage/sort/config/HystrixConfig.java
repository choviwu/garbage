package top.choviwu.garbage.sort.config;


import com.netflix.hystrix.contrib.javanica.aop.aspectj.HystrixCommandAspect;
import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HystrixConfig {


    @Bean
    public HystrixCommandAspect commandAspect(){
        return new HystrixCommandAspect();
    }


    @Bean
    public ServletRegistrationBean hystrixRegistration(){
        ServletRegistrationBean servletRegistrationBean =  new ServletRegistrationBean(new HystrixMetricsStreamServlet());
        servletRegistrationBean.addUrlMappings("/hystrix.stream");

        return servletRegistrationBean;
    }

}
