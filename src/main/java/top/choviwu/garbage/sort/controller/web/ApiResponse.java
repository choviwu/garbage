package top.choviwu.garbage.sort.controller.web;

import cn.hutool.json.JSONUtil;
import lombok.*;
import lombok.experimental.Accessors;

@Data
@Builder
@Accessors(chain = true)
@RequiredArgsConstructor
@AllArgsConstructor
public class ApiResponse {

    private Object data;
    private String msg;
    private int code;


    public static ApiResponse error(String msg){
        ApiResponse response = new ApiResponse();
        response.setCode(500);
        response.setData(msg);
        response.setMsg(Flag.FAIL.getStr());
        return response;
    }
    enum Flag{
        SUCCESS("success"),
        FAIL("fail")

        ;
        @Getter
        @Setter
        private String str;
        Flag(String str){
            this.str = str;
        }

    }

}
