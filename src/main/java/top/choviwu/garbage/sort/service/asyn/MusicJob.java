package top.choviwu.garbage.sort.service.asyn;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import top.choviwu.garbage.sort.entity.Music;
import top.choviwu.garbage.sort.mapper.MusicMapper;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/***
 *
 **/
@Component
public class MusicJob {


    @Autowired
    private MusicMapper musicMapper;

    @Scheduled(fixedRate = 60*2*60*1000L)
    public void deleteMusic(){

        List<Music> list = musicMapper.selectList(new QueryWrapper<Music>().lt("addtime", DateUtils.addMinutes(new Date(),-10)));
        System.out.println(list);
        if(!list.isEmpty()){
            list.forEach(c->{
                c.setState(-1);
                musicMapper.updateById(c);
            });
        }
    }

}
