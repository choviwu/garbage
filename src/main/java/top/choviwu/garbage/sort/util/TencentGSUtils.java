package top.choviwu.garbage.sort.util;

import cn.hutool.core.codec.Base64Encoder;
import cn.hutool.core.map.MapUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import top.choviwu.garbage.sort.constant.SmallConstant;

import java.util.Map;
@Component
public class TencentGSUtils {



    //设置APPID/AK
    @Value("${tencent.appid}")
    private String appid;
    @Autowired
    ReflectUtils reflectUtils;
//    app_id	是	int	正整数	1000001	应用标识（AppId）
//    time_stamp	是	int	正整数	1493468759	请求时间戳（秒级）
//    nonce_str	是	string	非空且长度上限32字节	fa577ce340859f9fe	随机字符串
//    sign	是	string	非空且长度固定32字节		签名信息，详见接口鉴权
//    format	是	int	正整数	1	图片格式，定义见下文描述
//    topk	是	int	[1, 5]	1	返回结果个数（已按置信度倒排）
//    image	是	string	原始图片的base64编码数据（解码后大小上限1MB）	...	待识别图片

//    将<key, value>请求参数对按key进行字典升序排序，得到有序的参数对列表N
//    将列表N中的参数对按URL键值对的格式拼接成字符串，得到字符串T（如：key1=value1&key2=value2），URL键值拼接过程value部分需要URL编码，URL编码算法用大写字母，例如%E8，而不是小写%e8
//    将应用密钥以app_key为键名，组成URL键值拼接到字符串T末尾，得到字符串S（如：key1=value1&key2=value2&app_key=密钥)
//    对字符串S进行MD5运算，将得到的MD5值所有字符转换成大写，得到接口请求签名
    public   String getVisionObj(String path){

        String post = SmallConstant.Tencent.VISION_OBJECTR.getValue();
        Map map = MapUtil.newHashMap();
        map.put("app_id",appid);
        map.put("time_stamp",System.currentTimeMillis()/1000);
        map.put("nonce_str",Math.random());
//        map.put("key1",SmallConstant.Tencent.APP_KEY.getValue());
        map.put("format",1);
        map.put("topk",2);
        map.put("image",Base64Encoder.encode(path));

        String sign = reflectUtils.getSignature(map);
        map.put("sign",sign);
        String result  =  HttpUtils.post(post,map,null);
        System.out.println(result);
        return result;
    }


    public static void main(String[] args) {
       // getVisionObj("https://blog.eunji.cn/upload/2019/2/qrcode_for_gh_1f51a05d7437_3442019030613133655.jpg");
    }
}
